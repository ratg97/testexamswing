package view;

import controller.MenuController;

import utils.ViewUtils;

public class MenuView extends javax.swing.JFrame {

    private MenuController menuController;

    /**
     * Creates new form MenuView
     */
    public MenuView() {
        initComponents();
        ViewUtils.setSettingsMenuView(this, "Menú");
        this.menuController = new MenuController(this);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        play = new javax.swing.JButton();
        rules = new javax.swing.JButton();
        create = new javax.swing.JButton();
        exit = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(300, 400));
        setPreferredSize(new java.awt.Dimension(300, 400));
        getContentPane().setLayout(new java.awt.GridLayout(4, 0));

        play.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/pencil.png"))); // NOI18N
        play.setText("Realizar examen");
        play.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        play.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        play.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                playActionPerformed(evt);
            }
        });
        getContentPane().add(play);

        rules.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/rules.png"))); // NOI18N
        rules.setText("Normas");
        rules.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        rules.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        rules.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rulesActionPerformed(evt);
            }
        });
        getContentPane().add(rules);

        create.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/file.png"))); // NOI18N
        create.setText("Crear examen");
        create.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        create.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        create.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                createActionPerformed(evt);
            }
        });
        getContentPane().add(create);

        exit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/cancel-2.png"))); // NOI18N
        exit.setText("Salir");
        exit.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        exit.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        exit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exitActionPerformed(evt);
            }
        });
        getContentPane().add(exit);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void playActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_playActionPerformed
        this.menuController.examView();
    }//GEN-LAST:event_playActionPerformed

    private void rulesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rulesActionPerformed
        new RulesView(this,true).setVisible(true);
    }//GEN-LAST:event_rulesActionPerformed

    private void createActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_createActionPerformed
        this.menuController.endView();
        this.menuController.createView();
    }//GEN-LAST:event_createActionPerformed

    private void exitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exitActionPerformed
        this.menuController.endView();
    }//GEN-LAST:event_exitActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton create;
    private javax.swing.JButton exit;
    private javax.swing.JButton play;
    private javax.swing.JButton rules;
    // End of variables declaration//GEN-END:variables
}
