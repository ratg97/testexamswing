package main;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.UIManager;
import utils.Constants;
import utils.PDFHeaderFooter;
import view.MenuView;
import view.Splash;

public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        if (args.length == 1 && args[0].equals("-h")) {
            System.out.println(
                    PDFHeaderFooter.END_MESSAGE + " mediante Java y las librerías JDOM e iText PDF.\n"
                    + "Asignatura Marcas. Curso 2016-2017. IES Riberde Castilla. Valladolid.");
        } else {
            /* Set the Nimbus look and feel */
            //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
            /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
             */
            try {
                for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                    if ("Nimbus".equals(info.getName())) {
                        javax.swing.UIManager.setLookAndFeel(info.getClassName());
                        break;
                    }
                }
            } catch (ClassNotFoundException ex) {
                java.util.logging.Logger.getLogger(MenuView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            } catch (InstantiationException ex) {
                java.util.logging.Logger.getLogger(MenuView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            } catch (IllegalAccessException ex) {
                java.util.logging.Logger.getLogger(MenuView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            } catch (javax.swing.UnsupportedLookAndFeelException ex) {
                java.util.logging.Logger.getLogger(MenuView.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
            }
            //</editor-fold>

            /* Create and display the form */
            java.awt.EventQueue.invokeLater(new Runnable() {
                public void run() {
                    Splash s = new Splash();
                    s.setVisible(true);
                    try {
                        Thread.sleep(Constants.TIME_SLASH);
                    } catch (InterruptedException ex) {
                        Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    s.dispose();
                    try {
                        //Tema del PC actual
                        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
                        new MenuView().setVisible(true);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                }
            });
        }
    }
}
