package controller;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JOptionPane;
import model.Question;
import utils.Constants;
import utils.XMLUtils;
import view.CreateExamView;
import view.ExamView;
import view.MenuView;

public class MenuController {

    private MenuView menuView;

    public MenuController(MenuView menuView) {
        this.menuView = menuView;
    }

    public void endView() {
        this.menuView.dispose();
    }

    public void examView() {
        ArrayList<Question> ques = new ArrayList<Question>();
        XMLUtils.read(ques,true);
        if (!ques.isEmpty()) {
            endView();
            String name = studentName();
            if (!(name == null)) {
                new ExamView(ques, name).setVisible(true);
            } else {
                new MenuView().setVisible(true);
            }
        }
    }

    public String studentName() {
        // Primera vez...
        // Login
        boolean exit = false;
        String nameStudent;
        do {
            nameStudent = JOptionPane.showInputDialog(null, "Introduce tu nombre", "Datos del alumno", JOptionPane.INFORMATION_MESSAGE);
            if (nameStudent == null) {
                exit = true;
            } else {
                exit = checkNameStudent(nameStudent);
            }
        } while (!exit);
        //Cancelar o exit...
        return (exit && nameStudent == null ? null : nameStudent);
    }

    public boolean checkNameStudent(String nameView) {
        Pattern pat = Pattern.compile(Constants.REGEX_NAME_STUDENT);
        Matcher mat = pat.matcher("" + nameView);
        return mat.matches();
    }

    public void createView() {
        new CreateExamView().setVisible(true);
    }
}
