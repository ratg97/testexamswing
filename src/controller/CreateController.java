package controller;

import java.io.File;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import model.Question;
import utils.Constants;
import utils.XMLUtils;
import view.CreateExamView;
import view.MenuView;

public class CreateController {

    private ArrayList<Question> qList;
    private CreateExamView createExamView;

    public CreateController(ArrayList<Question> qList, CreateExamView createExamView) {
        this.qList = qList;
        this.createExamView = createExamView;
    }

    public void addQuestion(Question q) {
        this.qList.add(q);
    }

    public void newQuestion(Question q, int posRemove) {
        this.qList.set(posRemove, q);
    }

    public boolean checkTyped(char c, String s) {
        s = s.trim();
        return !checkString(String.valueOf(c), Constants.REGEX_TYPED) || (s.length() > Constants.MAX_LENGTH_WRITE_QUESTION);
    }

    public boolean checkString(String s, String regex) {
        Pattern pat = Pattern.compile(regex);
        Matcher mat = pat.matcher(s);
        return mat.matches();
    }

    public boolean checkRepeatString(String s, String s2) {
        return s.equalsIgnoreCase(s2);
    }

    public ArrayList<Question> getqList() {
        return qList;
    }

    public String trimString(String s) {
        return s.trim();
    }

    public boolean isQuestionRepeat(Question q) {
        for (int i = 0; i < this.qList.size(); i++) {
            if (this.qList.get(i).getName().equalsIgnoreCase(q.getName())) {
                return true;
            }
        }
        return false;
    }

    public long getPositionQuestion(Question q) {
        for (int i = 0; i < this.qList.size(); i++) {
            if (this.qList.get(i).getName().equalsIgnoreCase(q.getName())) {
                return i;
            }
        }
        return -1;
    }

    //Mensajes//
    public String errorAdd() {
        return "Algún campo está vacío o falta por rellenar.";
    }

    public String errorAddTitle() {
        return "Algún campo está vacío o falta por rellenar.";
    }

    public String errorDelete() {
        return "No puedes eliminar si no seleccionas una.";
    }

    public String errorDeleteTitle() {
        return "Error al borrar";
    }

    public String errorRepeatAdd() {
        return "Esa pregunta ya existe.";
    }

    public String errorRepeatAddTitle() {
        return "Pregunta repetida";
    }

    public String errorCreate() {
        return "Tienes que introducir al menos 1 pregunta.";
    }

    public String errorCreateTitle() {
        return "Preguntas insuficientes";
    }
    
    public String confirmImport(){
    return "Se borrarán todos los datos de la tabla actuales...\n\n¿De verdad quieres continuar?\n\nSi añades un archivo erroneo se perderán\nlos datos igualmente.";
    }

    public boolean checkNQuestions() {
        return this.qList.isEmpty();
    }

    public void importExam() {
        this.qList.clear();
        XMLUtils.read(qList, false);
    }

    public void createExam() {
        JFileChooser chooser = new JFileChooser();
        chooser.setFileFilter(new FileNameExtensionFilter("XML Files", "xml"));
        chooser.setCurrentDirectory(new File(""));
        chooser.setDialogTitle("Guardar como un fichero xml que contiene el examen");
        int rc = chooser.showSaveDialog(null);

        if (rc == JFileChooser.APPROVE_OPTION) {
            File s = chooser.getSelectedFile().getName().endsWith(".xml") ? chooser.getSelectedFile() : new File(chooser.getSelectedFile() + ".xml");
            if (s.exists()) {
                int overw = JOptionPane.showConfirmDialog(null, "Ese archivo existe queires sobreescribirlo?");
                if (overw == JOptionPane.YES_OPTION) {
                    XMLUtils.write(this.qList, s);
                }
            } else {
                XMLUtils.write(this.qList, s);
            }
        }
    }

    public void exit() {
        new MenuView().setVisible(true);
        this.createExamView.dispose();
    }
}
