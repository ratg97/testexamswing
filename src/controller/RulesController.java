package controller;

import view.RulesView;

public class RulesController {

    private RulesView rulesView;

    public RulesController(RulesView rulesView) {
        this.rulesView = rulesView;
    }

    public String viewTitle() {
        return "Normas";
    }

    public String exam() {
        return html("Para realizar el examen tiene que cumplirse 2 condiciones:<ul><li>Fichero de preguntas correcto. Por defecto se cargará 'exam.xml' si existe en el mismo directorio de la aplicación, sino se tendra que escoger del dispositivo.<br/></li><li>Una vez verificado el fichero se tendrá que introducir el nombre del alumno</li></ul>");
    }

    public String grade() {
        return html("La nota final se compturará en base a:<ul><li>Un acierto: 10 entre el nºpreguntas.</li><li>Un fallo: mismo valor (-) que un acierto y además</li><li>Penalización: la mitad de un acierto.</li><li>En blanco: no sumará y no penalizará.</li></ul>");
    }

    public String result() {
        return html("Tras acabar el examen se generará automáticamente un PDF con los resultados en el mismo directorio de la aplicación.");
    }

    public String html(String s) {
        return "<html>" + s + "</html>";
    }
}
