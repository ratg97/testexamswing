package controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import model.Question;
import model.Student;
import utils.Constants;
import utils.PDFUtils;
import utils.XMLUtils;
import view.ExamView;
import view.MenuView;

public class ExamController {

    private ArrayList<Question> q;
    private Student s;
    private ExamView v;

    private int nQuestion;
    private int x;
    private int j;
    private int k;

    private String[] solutions;

    public ExamController(ArrayList<Question> q, Student s, ExamView v) {
        this.q = q;
        this.s = s;
        this.v = v;

        setQuestions();

        this.nQuestion = 0;
        this.solutions = new String[this.q.size()];
    }

    private void setQuestions() {
        // Las revolvemos aleatoriamente
        long seed = System.nanoTime();
        Collections.shuffle(this.q, new Random(seed));
        // Además las 3 respuestas también
        for (int i = 0; i < this.q.size(); i++) {
            Collections.shuffle(Arrays.asList(this.q.get(i).getAnswers()), new Random(seed));
        }
    }

    public void setStudent(String nameView) {
        this.s.setName(nameView);
        this.s.setGrade(Constants.DEFAULT_GRADE_START_EXAM);
    }

    public void addAnswer(String answerView) {
        this.solutions[this.nQuestion] = answerView;
    }

    public int getnQuestion() {
        return nQuestion;
    }

    public String[] getSolutions() {
        return solutions;
    }

    public void setnQuestion(int nQuestion) {
        this.nQuestion = nQuestion;
    }

    public ArrayList<Question> getQ() {
        return q;
    }

    public Student getS() {
        return s;
    }

    // Controlar el jRadioButton
    public void setXJK(String answerView) {
        if (this.q.get(this.nQuestion).getAnswers()[0].equalsIgnoreCase(answerView)) {
            this.x++;
            this.j = 0;
            this.k = 0;
        } else if (this.q.get(this.nQuestion).getAnswers()[1].equalsIgnoreCase(answerView)) {
            this.x = 0;
            this.j++;
            this.k = 0;
        } else if (this.q.get(this.nQuestion).getAnswers()[2].equalsIgnoreCase(answerView)) {
            this.x = 0;
            this.j = 0;
            this.k++;
        }
    }

    public int getX() {
        return x;
    }

    public int getJ() {
        return j;
    }

    public int getK() {
        return k;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setJ(int j) {
        this.j = j;
    }

    public void setK(int k) {
        this.k = k;
    }

    public void updateNextView() {
        this.v.updateQuestion(1);
    }

    public void updateBackView() {
        this.v.updateQuestion(-1);
    }
    
    public void backMenu() {
        new MenuView().setVisible(true);
    }

    public void exit(JFrame j) {
        j.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        backMenu();
        this.v.dispose();
    }
    
    // PDF
    public void generatePDF() {
        this.v.setVisible(false);
        PDFUtils.write(this.s, this.solutions, this.q, this.q.size());
    }

    //Mensajes
    public String firstQuestionBack() {
        return "Es la primera pregunta, no hay anteriores.";
    }

    public String exitMessage() {
        return "Estás realizando un examen, no puedes salir.";
    }

    public String finish() {
        return "Examen finalizado!!!\nSe ha generado un PDF con el resultado en la ruta:\n" + Constants.RESULT.getAbsolutePath();
    }

    public String errorTitle() {
        return "Error";
    }

    public String finishTitle() {
        return "Fin del examen";
    }

    // Para que las 3 soluciones salgan aleatorias siempre
    public int randomPosAnswer() {
        return (int) (Math.random() * 3);
    }
}
