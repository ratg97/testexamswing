package model;

public class Question {

    private String name;
    private String[] answers;
    private String solution;

    public Question() {
    }

    public Question(String name, String[] answers, String solution) {
        super();
        this.name = name;
        this.answers = answers;
        this.solution = solution;
    }

    public String getName() {
        return name;
    }

    public void setQuestion(String name) {
        this.name = name;
    }

    public String[] getAnswers() {
        return answers;
    }

    public void setAnswers(String[] answers) {
        this.answers = answers;
    }

    public String getSolution() {
        return solution;
    }

    public void setSolution(String solution) {
        this.solution = solution;
    }

    @Override
    public String toString() {
        return this.name+":"+this.answers[0]+":"+this.answers[1]+":"+this.answers[2]+":"+this.solution;
    }
    
    
}
