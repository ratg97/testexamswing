package utils;

import javax.swing.JDialog;
import javax.swing.JFrame;

public class ViewUtils {

    private ViewUtils() {
    }

    public static void setSettingsMenuView(JFrame j, String t) {
        // No puedes cambiar el tamaño de la ventana
        j.setResizable(false);
        // Ventana en el medio
        j.setLocationRelativeTo(null);
        //Título de la ventana
        j.setTitle(t);
    }

    public static void setSettingsRulesView(JDialog jd, String t) {
        // No puedes cambiar el tamaño de la ventana
        jd.setResizable(false);
        // Ventana en el medio
        jd.setLocationRelativeTo(null);
        //Título de la ventana
        jd.setTitle(t);
    }

    public static void setSettingsExamView(JFrame j) {
        // No puedes cambiar el tamaño de la ventana
        j.setResizable(false);
        // Ventana en el medio
        j.setLocationRelativeTo(null);
        // Bloquear cerrar la ventana
        j.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
    }
}
