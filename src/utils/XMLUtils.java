package utils;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

import model.Question;

public class XMLUtils {

    private XMLUtils() {

    }

    private static boolean checkRead(File defaultF) {
        return defaultF.exists();
    }

    private static void readXML(File readFile, ArrayList<Question> questionsFromXMLFile) {
        SAXBuilder builder = new SAXBuilder();
        try {
            Document document = (Document) builder.build(readFile);
            Element rootNode = document.getRootElement();
            boolean firstTime = true;
            List<Element> questionsList = rootNode.getChildren("question");
            for (int i = 0; i < questionsList.size(); i++) {
                Element questionsNode = questionsList.get(i);
                String name = questionsNode.getChildText("name");

                String[] answersArray = new String[3];
                for (int z = 0; z < answersArray.length; z++) {
                    answersArray[z] = questionsNode.getChildText("answer" + (z + 1));
                }
                String solution = questionsNode.getChildText("solution");
                int z = 0;
                boolean encontrado = false;
                while (!encontrado && z < answersArray.length) {
                    if (solution.equalsIgnoreCase(answersArray[z])) {
                        encontrado = true;
                    }
                    z++;
                }
                if (!encontrado) {
                    JOptionPane.showMessageDialog(null, "Error al leer en la pregunta " + (i + 1)
                            + ": la solución no se encuentra entre las respuestas posibles...", "Error", JOptionPane.ERROR_MESSAGE);
                    System.exit(0);
                }
                if (firstTime) {
                    questionsFromXMLFile.add(new Question(name, answersArray, solution));
                    firstTime = false;
                } else {
                    Question q = new Question(name, answersArray, solution);
                    if (!checkQuestionRepeat(questionsFromXMLFile, q)) {
                        questionsFromXMLFile.add(q);
                    } else {
                        JOptionPane.showMessageDialog(null, "La pregunta " + (i + 1) + " está repetida... se ignorará", "Error", JOptionPane.ERROR_MESSAGE);
                    }
                }
            }
        } catch (Exception io) {
            //  io.printStackTrace();
        }
    }

    public static void read(ArrayList<Question> questionsFromXMLFile, boolean defaultFile) {

        File fRead = defaultFile ? new File(Constants.EXAM_DEFAULT):null;
        if (fRead!=null && checkRead(fRead)) {
            readXML(fRead, questionsFromXMLFile);
        } else {
            JFileChooser chooser = new JFileChooser();
            chooser.setDialogTitle("Selecciona el fichero xml que contiene el examen");
            chooser.setFileFilter(new FileNameExtensionFilter("XML Files", "xml"));
            int rc = chooser.showOpenDialog(null);
            if (rc == JFileChooser.APPROVE_OPTION) {
                File newFile = chooser.getSelectedFile().getName().endsWith(".xml") ? chooser.getSelectedFile() : new File(chooser.getSelectedFile() + ".xml");
                readXML(newFile, questionsFromXMLFile);
                if (questionsFromXMLFile.isEmpty()) {
                    JOptionPane.showMessageDialog(null, "El fichero seleccionado no cumple el esquema de un examen.", "Error del fichero", JOptionPane.ERROR_MESSAGE);
                }
            }
        }

    }

    private static boolean checkQuestionRepeat(ArrayList<Question> questionsFromXMLFile, Question q) {
        for (Question que : questionsFromXMLFile) {
            if (que.getName().equalsIgnoreCase(q.getName())) {
                return true;
            }
        }
        return false;
    }

    public static void write(ArrayList<Question> questions, File path) {
        try {
            int len;
            Element trivial = new Element("exam");
            Document doc = new Document(trivial);
            int size = questions.size();
            for (int i = 0; i < size; i++) {
                Element question = new Element("question");
                Element name = new Element("name").setText(questions.get(i).getName());
                question.addContent(name);
                len = questions.get(i).getAnswers().length;
                for (int x = 0; x < len; x++) {
                    Element answer = new Element("answer" + (x + 1))
                            .setText(questions.get(i).getAnswers()[x]);
                    question.addContent(answer);
                }
                Element solution = new Element("solution").setText(questions.get(i).getSolution());
                question.addContent(solution);

                doc.getRootElement().addContent(question);
            }
            FileOutputStream fos = new FileOutputStream(path);
            Format format = Format.getPrettyFormat();
            format.setEncoding("UTF-8");
            XMLOutputter outputter = new XMLOutputter(format);
            outputter.output(doc, fos);
        } catch (Exception e) {
            JOptionPane.showInputDialog(null, e.getMessage(), "Error", JOptionPane.ERROR);
        }
    }

}
