package utils;

import java.io.File;

public class Constants {

    private Constants() {
    }

    public static final long TIME_SLASH = 2000;

    public static final String EXAM_DEFAULT = "exam.xml";

    public static final File RESULT = new File("result.pdf");

    public static final String REGEX_NAME_STUDENT = "[a-zA-ZÁÉÍÓÚÑáéíóúüñ]{1,10}";

    public static final int MAX_GRADE = 10;

    public static final int DEFAULT_GRADE_START_EXAM = 0;

    public static final int EMPTY_QUESTION_VALUE = 0;

    public static final int MAX_LENGTH_WRITE_QUESTION = 25;

    public static final String REGEX_QUESTION = "[\\wÁÉÍÓÚÑáéíóúüñ?¿¡! ]{1," + MAX_LENGTH_WRITE_QUESTION + "}";

    public static final String REGEX_TYPED = "[\\wÁÉÍÓÚÑáéíóúüñ?¿¡! ]{1,1}";

    //Por si falla el fichero al cargar
    public static final String OPTION1 = "Crear nuevo examen";
    public static final int OPTION1_VALUE = 1;

}
