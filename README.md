# **Proyecto NetBeans** #

Un ejemplo de un examen tipo test.
Recoge de un XML las x preguntas y una vez realizado el examen genera un PDF con el resultado.

![Captura8.PNG](https://bitbucket.org/repo/644Kegy/images/1915652007-Captura8.PNG)
![Captura.PNG](https://bitbucket.org/repo/644Kegy/images/604423091-Captura.PNG)

# **Metodología:** #

1. Swing

1. Todo aleatorio, tanto el orden de las preguntas como las respuestas

1. Patrones

1. Constantes

1. MVC

1. Expresiones regulares

1. Librería XML: jdom-2.0.6.jar 

1. Librería PDF: itextpdf-5.5.3.jar