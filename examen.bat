::@ECHO OFF
set JAVA_HOME="C:\Program Files\Java\jdk1.8.0_101\bin\java"
:: Se deben configurar la variables de entorno
set EXAMEN_HOME=C:\Users\ratg9\OneDrive\Documentos\NetBeansProjects\testexamswing

:: Librerias
set CLASSPATH_EXAMEN=%EXAMEN_HOME%\lib\ExamSwing.jar
set CLASSPATH_EXAMEN=%CLASSPATH_EXAMEN%;%EXAMEN_HOME%\lib\jdom-2.0.6.jar
set CLASSPATH_EXAMEN=%CLASSPATH_EXAMEN%;%EXAMEN_HOME%\lib\itextpdf-5.5.3.jar

:: Ejecucion
%JAVA_HOME% -cp %CLASSPATH_EXAMEN% main.Main %1
